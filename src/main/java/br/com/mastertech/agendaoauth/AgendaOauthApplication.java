package br.com.mastertech.agendaoauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendaOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendaOauthApplication.class, args);
	}

}
