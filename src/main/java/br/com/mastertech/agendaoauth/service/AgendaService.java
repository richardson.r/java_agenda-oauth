package br.com.mastertech.agendaoauth.service;

import br.com.mastertech.agendaoauth.exceptions.AgendaNaoExisteException;
import br.com.mastertech.agendaoauth.model.Agenda;
import br.com.mastertech.agendaoauth.model.Contato;
import br.com.mastertech.agendaoauth.repository.AgendaRepository;
import br.com.mastertech.agendaoauth.repository.ContatoRepository;
import br.com.mastertech.agendaoauth.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AgendaService {

    @Autowired
    private AgendaRepository agendaRepository;

    @Autowired
    private ContatoRepository contatoRepository;

    public Agenda getById(Long id) {
        return agendaRepository.findById(id).orElseThrow(AgendaNaoExisteException::new);
    }

    public Agenda getAgenda(Usuario usuario) {
        return agendaRepository.findByNome(usuario.getName())
                .orElseThrow(AgendaNaoExisteException::new);
    }

    public Contato salvarContato(Contato contato, Usuario usuario) {
        Agenda agenda = new Agenda();
        agenda = agendaRepository.findByNome(usuario.getName()).orElse(agenda);

        agenda.setNome(usuario.getName());
        List<Contato> contatos = new ArrayList<Contato>();
        contatos.add(contato);
        agenda.setContatos(contatos);

        agendaRepository.save(agenda);
        return contato;
    }
}
