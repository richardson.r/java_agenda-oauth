package br.com.mastertech.agendaoauth.repository;

import br.com.mastertech.agendaoauth.model.Agenda;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgendaRepository extends CrudRepository<Agenda, Long> {
    Optional<Agenda> findByNome(String nome);
}
