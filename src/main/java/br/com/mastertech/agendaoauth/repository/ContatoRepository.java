package br.com.mastertech.agendaoauth.repository;

import br.com.mastertech.agendaoauth.model.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoRepository extends CrudRepository<Contato, Long> {
}
