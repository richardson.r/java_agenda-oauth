package br.com.mastertech.agendaoauth.controller;

import br.com.mastertech.agendaoauth.model.Agenda;
import br.com.mastertech.agendaoauth.model.Contato;
import br.com.mastertech.agendaoauth.security.Usuario;
import br.com.mastertech.agendaoauth.service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class AgendaController {

    @Autowired
    private AgendaService agendaService;

    @GetMapping("/contatos")
    public Agenda getAgenda(@AuthenticationPrincipal Usuario usuario){
        return agendaService.getAgenda(usuario);
    }

    @PostMapping("/contato")
    @ResponseStatus(HttpStatus.CREATED)
    public Contato criarContato(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario){
        return agendaService.salvarContato(contato,usuario);
    }
}
